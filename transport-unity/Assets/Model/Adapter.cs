using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class Adapter : IAdapter {
	public Map map;
	public TileClickState tileClickState;
	public BuildingType buildingType;
	public Tile lastTileClicked;
	public List<Leg> editingLegs;
	public List<Route> savedRoutes;
	public Text statusText;
	public Adapter () {
		Debug.Log("Adapter created");
		map = new Map();
		tileClickState = TileClickState.info;
		savedRoutes = new List<Route>();
		List<CommodityType> c1 = new List<CommodityType>();
		c1.Add(CommodityType.wood);
		Leg leg1 = new Leg(map, map.GetTile(2, 3), map.GetTile(3, 7), c1);
		List<CommodityType> c2 = new List<CommodityType>();
		c2.Add(CommodityType.paper);
		Leg leg2 = new Leg(map, map.GetTile(3, 7), map.GetTile(2, 3), c2);
		List<Leg> legs = new List<Leg>();
		legs.Add(leg1);
		legs.Add(leg2);
		Route r = new Route(legs);

		Wagon w = new Wagon();

		Container c = new Container(CommodityType.wood);
		c.tonnes = 30;
		w.containers.Add(c);

		w.currentTile = map.GetTile(2, 3);
		w.locationInTile = new Vector2Int(0, 0);
		w.currentLeg = leg1;
		w.route = r;

		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
		w.Step();
	}
	public void ShowStatus(string s) {
		statusText.text = s;
	}
	public Tile GetTile(int x, int y) {
		return map.GetTile(x, y);
	}
	public void OutputTerrain(Tile t) {
		switch (t.Terrain) {
		case TerrainType.woods:
			Debug.Log("woods");
			break;
		case TerrainType.water:
			Debug.Log("water");
			break;
		case TerrainType.plains:
			Debug.Log("plains");
			break;
		case TerrainType.mountain:
			Debug.Log("mountain");
			break;
		}
	}

	#region IAdapter

	public IEnumerable<Tile> Tiles() {
		return map.Tiles;
	}

	public IEnumerable<Route> Routes() {
		return savedRoutes;
	}

	public void SetStatusElement(Text element) {
		statusText = element;
	}

	public void TimePassed(float time) {
	}

	public void BuildingWasClicked(Vector2Int tileCoords) {
		Debug.Log("BuildingWasClicked on tile (" + tileCoords.x + "," + tileCoords.y + ")");
	}

	public void TrackWasClicked(Vector2Int tileCoords) {
		Debug.Log("TrackWasClicked on tile (" + tileCoords.x + "," + tileCoords.y + ")");
	}

	public Direction[] GetDirections(Tile t1, Tile t2) {
    	int deltaX, deltaY;
    	Direction[] dirs = new Direction[2];
    	deltaX = t2.Coords.x - t1.Coords.x;
    	deltaY = t2.Coords.y - t1.Coords.y;
    	if (Math.Abs(deltaX) > 1 || Math.Abs(deltaY) > 1) {
    		Debug.Log("Not neighbouring");
    		return new Direction[] {};
    	}
    	if (deltaX == -1) {
    		if (deltaY == -1) {
    			dirs[0] = Direction.SE;
    			dirs[1] = Direction.NW;
    		} else if (deltaY == 0) {
    			dirs[0] = Direction.E;
    			dirs[1] = Direction.W;
    		} else {
    			dirs[0] = Direction.NE;
    			dirs[1] = Direction.SW;
    		}
    	} else if (deltaX == 0) {
    		if (deltaY == -1) {
    			dirs[0] = Direction.S;
    			dirs[1] = Direction.N;
    		} else if (deltaY == 0) {
	    		Debug.Log("Same tile");
	    		return new Direction[] {};
			} else {
	   			dirs[0] = Direction.N;
    			dirs[1] = Direction.S;
			}
    	} else {
    		if (deltaY == -1) {
    			dirs[0] = Direction.SW;
    			dirs[1] = Direction.NE;
    		} else if (deltaY == 0) {
    			dirs[0] = Direction.W;
    			dirs[1] = Direction.E;
    		} else {
    			dirs[0] = Direction.NW;
    			dirs[1] = Direction.SE;
    		}    		
    	}
    	return dirs;
	}

	public bool TileWasClicked(Vector2Int tileCoords) {
		Vector2Int lastTileCoords = new Vector2Int();
		if (lastTileClicked != null) {
			lastTileCoords.x = lastTileClicked.Coords.x;
			lastTileCoords.y = lastTileClicked.Coords.y;
		}
		Debug.Log("TileWasClicked (" + tileCoords.x + "," + tileCoords.y + ")");
		Leg newLeg;
		Tile clickedTile = map.GetTile(tileCoords.x, tileCoords.y);
		if (tileClickState == TileClickState.route) {
			if (clickedTile.Terrain == TerrainType.water) {
				Debug.Log("Can't route on water");
				return false;
			}
			if (lastTileClicked != null) {
				newLeg = new Leg(map, lastTileClicked, clickedTile);
				editingLegs.Add(newLeg);
			}
			lastTileClicked = map.Tiles[tileCoords.y *Constants.TILE_COLS + tileCoords.x];
		} else if (tileClickState == TileClickState.building) {
			Debug.Log("Building type is " + buildingType);
			//Building b = (Building)Activator.CreateInstance(buildingType);
			if (clickedTile.Terrain == TerrainType.water) {
				Debug.Log("Can't build building on water");
				return false;
			}
			Building b = new Building();
			b.buildingType = buildingType;
			Debug.Log("b type is " + b.buildingType);
			map.Tiles[tileCoords.y *Constants.TILE_COLS + tileCoords.x] = clickedTile.AddBuilding(b);
			Debug.Log("Placed Building");
			ShowStatus("Placed Building");
			tileClickState = TileClickState.info;
		} else if (tileClickState == TileClickState.info) {
			ShowStatus("Clicked on tile " + clickedTile.GetString());
			if (clickedTile.building is Mine) {
				ShowStatus("Has building which is Mine");
			} else if (clickedTile.building != null) {
				ShowStatus("Other building type");
			} else {
				ShowStatus("No building");
			}
		} else if (tileClickState == TileClickState.tracks) {
			if (clickedTile.Terrain == TerrainType.water) {
				Debug.Log("Can't build tracks on water");
				return false;
			}
            if (lastTileClicked != null) {
            	Direction[] dirs = GetDirections(lastTileClicked, clickedTile);;
            	if (dirs.Length > 0) {
	                map.Tiles[tileCoords.y *Constants.TILE_COLS + tileCoords.x] = clickedTile.AddHalfTrack(dirs[0]);
	                map.Tiles[lastTileCoords.y *Constants.TILE_COLS + lastTileCoords.x] = lastTileClicked.AddHalfTrack(dirs[1]);
	            }
            }
            lastTileClicked = map.Tiles[tileCoords.y *Constants.TILE_COLS + tileCoords.x];
        }
        return true;
	}

	public void CreateNewRouteClicked() {
		Debug.Log("Clicked create new route");
		lastTileClicked = null;
		tileClickState = TileClickState.route;
		editingLegs = new List<Leg>();
	}

	public void EndRouteClicked() {
		Debug.Log("Clicked end route");
		tileClickState = TileClickState.info;
		if (editingLegs.Count == 0) {
			Debug.Log("No route created.");
			return;
		}
		Route r = new Route(editingLegs);
		savedRoutes.Add(r);
	}

	public void ClearRouteClicked() {
		Debug.Log("Clicked clear route");
		lastTileClicked = null;
		editingLegs = new List<Leg>();
		tileClickState = TileClickState.info;
	}

  public void AddMineClicked() {
		Debug.Log("Clicked add mine");
		tileClickState = TileClickState.building;
		buildingType = BuildingType.mine;
  }

  public void AddFactoryClicked() {
		Debug.Log("Clicked add factory");
		tileClickState = TileClickState.building;
		buildingType = BuildingType.forge;
  }

  public void AddRailroadTracksClicked() {
		Debug.Log("Clicked add railroad tracks");
		tileClickState = TileClickState.tracks;
  }

  #endregion
}
