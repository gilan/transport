using UnityEngine;

public class Building {
	public BuildingType buildingType {get; set;}
	public Tile tile {get; set;}
}