using UnityEngine;

public class Forge : Building {
	public new BuildingType buildingType {get; private set;}
	public Forge () {
		buildingType = BuildingType.forge;
	}
}