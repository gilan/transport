using UnityEngine;

public class Mine : Building {
	public new BuildingType buildingType {get; private set;}
	public Mine () {
		buildingType = BuildingType.mine;
	}
}