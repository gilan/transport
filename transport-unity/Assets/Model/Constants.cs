using System;

public enum TerrainType {plains, mountain, woods, water};
public enum BuildingType {none, forge, mine};
public enum CommodityType {wood, iron, grain, paper, steel, food};
public enum TileClickState {route, info, building, tracks};

[Flags]
public enum Direction {
	None = 0,
	NW = 1<<1,
	N  = 1<<2,
	NE = 1<<3, 
	W  = 1<<4, 
	E  = 1<<5, 
	SW = 1<<6, 
	S  = 1<<7, 
	SE = 1<<8 
}

public static class Constants {
	public static int TILE_ROWS = 16;
	public static int TILE_COLS = 16;
	public static int CONTAINER_TONNES = 40;
}
