using UnityEngine;

public class Container {
	public int tonnes {get; set;}
	public CommodityType commodityType {get; set;}
	public Container (CommodityType c) {
		Debug.Log("Created new " + c + " container");
	}
}