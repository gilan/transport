﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

public interface IAdapter {
	IEnumerable<Tile> Tiles();

	void TimePassed(float time);

	bool TileWasClicked(UnityEngine.Vector2Int tileCoords);

	void BuildingWasClicked(UnityEngine.Vector2Int tileCoords);

	void TrackWasClicked(UnityEngine.Vector2Int tileCoords);

	void CreateNewRouteClicked();

	void EndRouteClicked();

	void ClearRouteClicked();

    void AddMineClicked();

    void AddRailroadTracksClicked();

    void AddFactoryClicked();

  	void SetStatusElement(Text element);
}
