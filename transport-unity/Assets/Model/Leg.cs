using System.Collections.Generic;
using UnityEngine;

public class Leg {
/* one leg of a route, consisting of a starting tile, an ending tile, a list of commodities to carry from start to end, and a list of Tiles to pass through
*/
	public Map map;
	public List<Tile> Tiles {get; set;}
	public List<CommodityType> Loads;
	public Leg (Map m, Tile s, Tile e, List<CommodityType> l = null) {
		map = m;
		Debug.Log("Making Leg from (" + s.Coords.x + ", " + s.Coords.y + ") to (" + e.Coords.x + ", " + e.Coords.y + ")");
		Tiles = new List<Tile>();
		SetSimplePath(s, e);
		Loads = l;
	}
	public void SetSimplePath (Tile s, Tile e) {
		int x, y;
		Tile currentTile;
		currentTile = s;
		x = -1;
		y = -1;
		while (currentTile != e) {
			x = currentTile.Coords.x;
			y = currentTile.Coords.y;
			Tiles.Add(currentTile);
			Debug.Log("Added tile (" + x + ", " + y + ")");
			if (currentTile.Coords.x > e.Coords.x) {
				x--;
			} else if (currentTile.Coords.x < e.Coords.x) {
				x++;
			}
			if (currentTile.Coords.y > e.Coords.y) {
				y--;
			} else if (currentTile.Coords.y < e.Coords.y) {
				y++;
			}
			currentTile = map.GetTile(x, y);
		}
		Tiles.Add(e);
		Debug.Log("Added tile (" + x + ", " + y + ")");
	}
}
