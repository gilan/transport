using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class Layer {
	public int[] data;
}

[System.Serializable]
public class MapInfo {
		public Layer[] layers;
}

public class Map {
	public List<Tile> Tiles {get; set;}
	public Map () {
		int x, y, currentTile;
		StreamReader reader = new StreamReader("Assets/Maps/firstmap.json");
				string json = reader.ReadToEnd();
		MapInfo mapInfo = JsonUtility.FromJson<MapInfo>(json);
		currentTile = 0;
		Tiles = new List<Tile>();
		for (y = 0; y < Constants.TILE_ROWS; y++) {
			for (x = 0; x < Constants.TILE_COLS; x++) {
				int data = mapInfo.layers[0].data[currentTile];
				Tile t = new Tile(new Vector2Int(x, y), (TerrainType)(data-1));
				Tiles.Add(t);
				currentTile++;
			}
		}
	}

	public void SetTile(int x, int y, TerrainType t) {
		Tiles[y *Constants.TILE_COLS + x] = Tiles[y *Constants.TILE_COLS + x].SetTerrain(t);
	}

	public Tile GetTile(int x, int y) {
		return Tiles[y *Constants.TILE_COLS + x];
	}
}
