using System.Collections.Generic;
using UnityEngine;

public class Route {
	public int routeNumber;
	public static int nextRouteNumber = 1;
	public List<Leg> Legs {get; private set;}

	public Route (List<Leg> l = null) {
		if (l == null) {
			l = new List<Leg>();
		}
		Legs = l;
		routeNumber = nextRouteNumber;
		nextRouteNumber++;
		Debug.Log("created route " + routeNumber);
	}
	public override int GetHashCode () {
		return Legs.GetHashCode() + 2048 * routeNumber;
	}
	public override bool Equals (object o) {
		return this.Equals(o as Route);
	}
	public bool Equals(Route o) {
		return (routeNumber == o.routeNumber && Legs == o.Legs);
	}

}