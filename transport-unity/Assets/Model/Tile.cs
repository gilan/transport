using UnityEngine;
using System.Collections.Specialized;

public class Tile {
	public Vector2Int Coords {get; private set;}
	public TerrainType Terrain {get; private set;}
	public BuildingType buildingType {get; private set;}
	public Building building {get; private set;}
	public Direction halfTracks {get; private set;}

	public Tile (Vector2Int c, TerrainType _t, Building b, Direction hT) {
		Coords = c;
		Terrain = _t;
		building = b;
		if (hT != Direction.None) {
			Debug.Log("created Tile with halfTracks " + hT);
		}
		halfTracks = hT;
		if (building != null) {
			Debug.Log("created Tile with building type " + b.buildingType);
			buildingType = b.buildingType;
		} else {
			buildingType = BuildingType.none;
		}
	}
	public Tile (Vector2Int c, TerrainType _t, Building b): this(c, _t, b, Direction.None) {}
	public Tile (Vector2Int c, TerrainType _t): this(c, _t, null, Direction.None) {}
    public Tile copy() {
        var t = new Tile(Coords, Terrain, building, halfTracks);
        return t;
    }
	public override bool Equals (object o) {
		return this.Equals(o as Tile);
	}
	public bool Equals(Tile o) {
		return (building == o.building && Terrain == o.Terrain && Coords == o.Coords && halfTracks == o.halfTracks);
	}
	public override int GetHashCode () {
		return Coords.GetHashCode() + 2 * Terrain.GetHashCode() + 4 * building.GetHashCode() + 8 * halfTracks.GetHashCode();
	}
	public string GetString () {
		return "(" + Coords.x + ", " + Coords.y + ")";
	}
	public Tile SetTerrain (TerrainType _t) {
		return new Tile (Coords, _t, building);
	}
	public Tile AddBuilding (Building b) {
		Debug.Log("adding building of type " + b.buildingType);
		Tile t = new Tile (Coords, Terrain,  b);
		return t;
	}
	public Tile AddHalfTrack (Direction d) {
		Debug.Log("adding half track to " + Coords.x + "," + Coords.y + "," + d);
		Debug.Log("old halfTracks was " + halfTracks);
		Direction newHalfTracks = halfTracks | d;
		Debug.Log("New halfTracks is " + newHalfTracks);
		Tile t = new Tile (Coords, Terrain, building, newHalfTracks);
		return t;
	}
	public int CanProduce (CommodityType c) {
		if (c == CommodityType.iron && Terrain == TerrainType.mountain) {
			return 1;
		}
		return 0;
	}
	public int Produces (CommodityType c) {
		if (c == CommodityType.iron && Terrain == TerrainType.mountain && building is Mine) {
			return 1;
		}
		return 0;
	}
    public Tile addTrack(int neighbor) {
        // TODO
        return this;
    }
}
