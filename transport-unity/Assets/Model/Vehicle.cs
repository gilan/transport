using System.Collections.Generic;
using UnityEngine;

public abstract class Vehicle {
	public abstract int speed {get; set;}
	public abstract int containerCapacity {get; set;}
	public List<Container> containers {get; set;}
	public Tile currentTile {get; set;}
	public Vector2Int locationInTile {get; set;}
	public Leg currentLeg {get; set;}
	public Route route {get; set;}

	public void Step () {
		int currentTileIndex, currentLegIndex;
		Tile nextTile;
		currentTileIndex = currentLeg.Tiles.FindIndex(c => c == currentTile);
		if (currentTileIndex == currentLeg.Tiles.Count - 1) {
			currentLegIndex = route.Legs.FindIndex(c => c == currentLeg);
			if (currentLegIndex == route.Legs.Count - 1) {
				currentLeg = route.Legs[0];
			} else {
				currentLeg = route.Legs[currentLegIndex + 1];
			}
			nextTile = currentLeg.Tiles[0];
		} else {
			nextTile = currentLeg.Tiles[currentTileIndex + 1];
		}
		Debug.Log("Moving vehicle from tile " + currentTile.GetString() + " to " + nextTile.GetString());
		currentTile = nextTile;
	}
}