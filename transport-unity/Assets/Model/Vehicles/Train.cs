using System.Collections.Generic;
using UnityEngine;

public class Train : Vehicle {
	public override int speed {get; set;}
	public override int containerCapacity {get; set;}
	public Train () {
		speed = 2;
		containerCapacity = 4;
		Debug.Log("Created new Train");
		containers = new List<Container>();
	}
}