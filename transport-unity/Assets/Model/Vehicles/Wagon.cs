using System.Collections.Generic;
using UnityEngine;

public class Wagon : Vehicle {
	public override int speed {get; set;}
	public override int containerCapacity {get; set;}
	public Wagon () {
		speed = 1;
		containerCapacity = 1;
		Debug.Log("Created new Wagon");
		containers = new List<Container>();
	}
}