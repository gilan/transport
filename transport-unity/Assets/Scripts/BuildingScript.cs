using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingScript : MonoBehaviour {
  public Vector2Int TileCoords { get; set; }
  public ITileHandler Handler { get; set; }
  private Renderer buildingRenderer;

  private void Awake() {
    buildingRenderer = GetComponent<Renderer>();
  }

  public void SetBuildingType(BuildingType type) {
    buildingRenderer.material = Resources.Load<Material>("Materials/" + type.ToString());
  }

  private void OnMouseDown() {
    Handler.BuildingWasClicked(TileCoords);
  }
}
