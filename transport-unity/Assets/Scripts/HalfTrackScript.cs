using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HalfTrackScript : MonoBehaviour {
  public Vector2Int TileCoords { get; set; }
  public ITileHandler Handler { get; set; }
  private Renderer trackRenderer;

  private void Awake() {
    trackRenderer = GetComponent<Renderer>();
    trackRenderer.material = Resources.Load<Material>("Materials/track");
  }

  private void OnMouseDown() {
    Handler.TrackWasClicked(TileCoords);
  }
}
