﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ITileHandler {
  void TileWasClicked(UnityEngine.Vector2Int tileCoords);
  void BuildingWasClicked(UnityEngine.Vector2Int tileCoords);
  void TrackWasClicked(UnityEngine.Vector2Int tileCoords);
}
