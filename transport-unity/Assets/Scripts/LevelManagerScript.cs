using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LevelManagerScript : MonoBehaviour, ITileHandler {
	private IAdapter adapter;
	private List<Tile> tilesWas;
	private List<Tile> tilesNew;
	public Text statusText;
	private static TileScript [,] tileScripts;

	// Use this for initialization
	void Start () {
		adapter = new Adapter();
		tileScripts = new TileScript [Constants.TILE_ROWS, Constants.TILE_COLS];
		initiateMap(adapter.Tiles(), this);
		adapter.SetStatusElement(statusText);
	}

	private static void initiateMap(IEnumerable<Tile> tiles, ITileHandler handler) {
		Debug.Log("initiate Map");
		var parent = new GameObject();
		parent.name = "Tiles";

		var tileScriptPrefab = Resources.Load<GameObject>("Prefabs/Tile");
		foreach (var tile in tiles) {
			var tileScript = createTileScript(tile, tileScriptPrefab);
	    	tileScript.Handler = handler;
	    	tileScript.transform.parent = parent.transform;
		}
	}

	private static TileScript createTileScript(Tile tile, GameObject tileScriptPrefab) {
		var tileScriptObject = Instantiate(tileScriptPrefab);
		var tileScript = tileScriptObject.GetComponent<TileScript>();
		tileScript.SetTerrainType(tile.Terrain);
	    tileScript.TileCoords = tile.Coords;
		tileScriptObject.transform.position = positionFromTileCoords(tile.Coords[0], tile.Coords[1]);
		tileScriptObject.name = string.Format("Tile {0}.{1}", tile.Coords[0], tile.Coords[1]);
		tileScripts[tile.Coords[0], tile.Coords[1]] = tileScript;
		return tileScript;
	}

	private static Vector3 positionFromTileCoords(int x, int y) {
		return new Vector3(x * 10, 0, y * 10);
	}
	
	// Update is called once per frame
	void Update () {
		int i = 0;
		BuildingType bT;
		adapter.TimePassed(Time.deltaTime);
		IEnumerable<Tile> iTiles = adapter.Tiles();
		tilesNew = iTiles.ToList();
		if (tilesWas == null) {
			tilesWas = tilesNew;
		}
		foreach (var tile in tilesWas) {
			if (tile != tilesNew.ElementAt(i)) {
				if (tile.buildingType != tilesNew.ElementAt(i).buildingType) {
					Debug.Log("Tile " + i + " changed building type");
					tileScripts[tile.Coords[0], tile.Coords[1]].SetBuildingType(tilesNew.ElementAt(i).buildingType);
				} else if (tile.halfTracks != tilesNew.ElementAt(i).halfTracks) {
					tileScripts[tile.Coords[0], tile.Coords[1]].SetHalfTracks(tilesNew.ElementAt(i).halfTracks);
				}
			}
			i++;
		}
		tilesWas = tilesNew;
	}


  public void TileWasClicked(Vector2Int tileCoords) {
    adapter.TileWasClicked(tileCoords);
  }

  public void BuildingWasClicked(Vector2Int tileCoords) {
    adapter.BuildingWasClicked(tileCoords);
  }

  public void TrackWasClicked(Vector2Int tileCoords) {
    adapter.TrackWasClicked(tileCoords);
  }

  public void AddMineClicked() {
    adapter.AddMineClicked();
  }

  public void AddFactoryClicked() {
    adapter.AddFactoryClicked();
  }

  public void StartRouteClicked() {
    adapter.CreateNewRouteClicked();
  }

  public void EndRouteClicked() {
    adapter.EndRouteClicked();
  }

  public void CancelRouteClicked() {
    adapter.ClearRouteClicked();
  }

  public void AddRailroadTracksClicked() {
    adapter.AddRailroadTracksClicked();
  }
}
