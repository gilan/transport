﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileScript : MonoBehaviour {
  public Vector2Int TileCoords { get; set; }
  public ITileHandler Handler { get; set; }
  private Renderer terrainRenderer;
  private GameObject building;
  private GameObject factoryScriptPrefab;
  private GameObject mineScriptPrefab;
  private GameObject halfTrackScriptPrefab;

  private void Awake() {
    terrainRenderer = GetComponent<Renderer>();
    factoryScriptPrefab = Resources.Load<GameObject>("Prefabs/Factory");
    mineScriptPrefab = Resources.Load<GameObject>("Prefabs/Mine");
    halfTrackScriptPrefab = Resources.Load<GameObject>("Prefabs/HalfTrack");
  }

  public void SetTerrainType(TerrainType type) {
    terrainRenderer.material = Resources.Load<Material>("Materials/" + type.ToString());
  }

  public GameObject CreateBuilding (BuildingType type) {
    BuildingScript buildingScript = null;
    if (type == BuildingType.forge) {
      Debug.Log("forge");
      buildingScript = Instantiate(factoryScriptPrefab).GetComponent<FactoryScript>();
    } else if (type == BuildingType.mine) {
      Debug.Log("mine");
      buildingScript = Instantiate(mineScriptPrefab).GetComponent<MineScript>();
    }
    buildingScript.TileCoords = TileCoords;
    buildingScript.Handler = Handler;
    return buildingScript.gameObject; 
  }

  public void SetBuildingType(BuildingType type) {
    if (building != null) {
      Destroy(building);
    }
    building = CreateBuilding(type);
    building.transform.parent = gameObject.transform;
    building.transform.localPosition = new Vector3(0, 0, 0);
  }

  public void SetHalfTracks(Direction d) {
    HalfTrackScript[] halfTrackScript = new HalfTrackScript[8];
    HalfTrackScript nextScript;
    GameObject halfTrack = null;
    int nextIndex = 0;
    if (d == Direction.None) {
      return;
    }
    if ((d & Direction.N) != 0) {
      nextScript = Instantiate(halfTrackScriptPrefab).GetComponent<HalfTrackScript>();
      nextScript.TileCoords = TileCoords;
      nextScript.Handler = Handler;
      halfTrack = nextScript.gameObject;
      halfTrack.transform.parent = gameObject.transform;
      halfTrack.transform.localPosition = new Vector3(0, 0, -3.5f);
      halfTrack.transform.Rotate(0, 90f, 0);
      halfTrackScript[nextIndex] = nextScript;
      nextIndex++;
    }
    if ((d & Direction.S) != 0) {
      nextScript = Instantiate(halfTrackScriptPrefab).GetComponent<HalfTrackScript>();
      nextScript.TileCoords = TileCoords;
      nextScript.Handler = Handler;
      halfTrack = nextScript.gameObject;
      halfTrack.transform.parent = gameObject.transform;
      halfTrack.transform.localPosition = new Vector3(0, 0, 3.5f);
      halfTrack.transform.Rotate(0, 90f, 0);
      halfTrackScript[nextIndex] = nextScript;
      nextIndex++;
    }
    if ((d & Direction.W) != 0) {
      nextScript = Instantiate(halfTrackScriptPrefab).GetComponent<HalfTrackScript>();
      nextScript.TileCoords = TileCoords;
      nextScript.Handler = Handler;
      halfTrack = nextScript.gameObject;
      halfTrack.transform.parent = gameObject.transform;
      halfTrack.transform.localPosition = new Vector3(-3.5f, 0, 0);
      halfTrackScript[nextIndex] = nextScript;
      nextIndex++;
    }
    if ((d & Direction.E) != 0) {
      nextScript = Instantiate(halfTrackScriptPrefab).GetComponent<HalfTrackScript>();
      nextScript.TileCoords = TileCoords;
      nextScript.Handler = Handler;
      halfTrack = nextScript.gameObject;
      halfTrack.transform.parent = gameObject.transform;
      halfTrack.transform.localPosition = new Vector3(3.5f, 0, 0);
      halfTrackScript[nextIndex] = nextScript;
      nextIndex++;
    }
  }

  private void OnMouseDown() {
    Handler.TileWasClicked(TileCoords);
  }
}
